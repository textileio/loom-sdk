### Textile's Loom SDK

The Loom SDK allow you to easily enable the Textile data streams from your mobile applications. For experienced developers, adding the SDK to your mobile app shouldn't take more than a few minutes. We've kept the integration simple and the footprint small (<500kb). Once installed, your mobile app can begin delivering high quality data to your data scientists without any more work.

For installation instructions, see [CocoaPods](http://cocoapods.org/pods/Loom).

All documentation is available on [docs.textile.io](http://docs.textile.io/)

Any technical issues can be reported here as [Issues](https://gitlab.com/weareset/Loom-SDK/issues).
